package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class ControlTest {
    @Test
    public void testDivision() throws Exception {

        int k= new Control().division(10,2);
        
        assertEquals("Division 5",5,k);

    }
    @Test
    public void testDivisionWithZero() throws Exception {

        int k= new Control().division(10,0);
        
        assertEquals("Max Integer",Integer.MAX_VALUE,k);

    }
}